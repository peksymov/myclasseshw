class Employee {
    constructor(name,age,salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    // ==== ==== ==== getters ==== ==== ==== //
    get name () {
        return this._name;
    }
    get age () {
        return this._age;
    }
    get salary () {
        return this._salary;
    }
    // ==== ==== ==== setters ==== ==== ==== //
    set name(value) {
        this._name = value;
    }
    set age(value) {
        this._age = value;
    }
    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(name,age,salary,language = "Ukrainian") {
        super(name,age,salary);
        this.language = language;
    }
    set salary(value) {
        this._salary = value;
    }
    get salary () {
        return this._salary * 3;
    }
}

// const eew = new Employee("Serg", 12, 787877);
// console.log(eew);

// -------------------Examples----------------------------------Examples--------------------------------- //
const pr = new Programmer("Vasia",23, 20000, "English");
console.log(pr);
console.log(pr.salary);

const prp = new Programmer("kolia",26, 70000, );
console.log(prp);
console.log(prp.salary);

const seer = new Programmer("Sania", 55, 35000, "Russian");
console.log(seer);
console.log(seer.salary);
// -------------------Examples----------------------------------Examples--------------------------------- //